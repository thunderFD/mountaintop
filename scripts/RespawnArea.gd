extends Area

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(String) var respawn_point = "RespawnPoint"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_RespawnArea_body_entered(body):
	if body.name == "Player":
		body.transform = get_node(respawn_point).global_transform
		
