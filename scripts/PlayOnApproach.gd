extends AudioStreamPlayer3D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(float) var radius = 25
export(String) var player_path = "../../Player"
var player_node: Spatial

# Called when the node enters the scene tree for the first time.
func _ready():
	player_node = get_node(player_path)
	if player_node == null:
		printerr("couldn't find Player node!")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var player_pos = player_node.translation
	var distance = translation.distance_to(player_pos)
	if !self.playing && (distance < radius):
		print("play music!")
		self.play()
